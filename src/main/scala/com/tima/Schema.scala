package com.tima

import scala.collection.mutable

case class Schema(tableName: String, columnFamily: String, key: String, columns: mutable.Map[String, String]) {
    def getHBaseCatalog =
        s"""{
           |"table":{"namespace":"default", "name":"$tableName"},
           |"rowkey" : "key",
           |$makeColumn
           |}
        """.stripMargin

    def makeColumn: String = {
        var cols = mutable.ArrayBuffer[String]()
        for ((columnName, columnType) <- columns) {
            if (columnName.equals(key)) {
                cols += s""""$columnName":{"cf":"rowkey","col":"key","type":"$columnType"}"""
            } else {
                cols += s""""$columnName":{"cf":"$columnFamily","col":"$columnName","type":"$columnType"}"""
            }
        }
        s""""columns":{
           |${cols.mkString(",\n")}
           |}""".stripMargin
    }
}
