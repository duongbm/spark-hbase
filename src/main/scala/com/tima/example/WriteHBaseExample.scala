package com.tima.example

import com.tima.spark.Spark
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.execution.datasources.hbase.HBaseTableCatalog

object WriteHBaseExample extends Spark with App {
    val spark: SparkSession = getSparkSession(new SparkConf().setMaster("local[2]"))

    def catalog =
        s"""{
           |"table":{"namespace":"default", "name":"spark"},
           |"rowkey":"key",
           |"columns":{
           |"uid":{"cf":"rowkey", "col":"key", "type":"string"},
           |"name":{"cf":"info", "col":"name", "type":"string"}
           |}
           |}""".stripMargin

    import spark.implicits._

    val df = spark.read.option("header", "true").csv("test.csv")
    //    val df = spark.sparkContext.parallelize(Seq(("3", "datlt"), ("4", "tupn"))).toDF("rowkey", "name")
    df.write.options(Map(HBaseTableCatalog.tableCatalog -> catalog, HBaseTableCatalog.newTable -> "5")).format("org.apache.spark.sql.execution.datasources.hbase").save()
}
