package com.tima.example

import com.tima.spark.Spark
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.execution.datasources.hbase.HBaseTableCatalog

object ReadHBaseExample extends Spark {
    val sparkConf = new SparkConf()
    sparkConf.setMaster("local[2]")
    val spark: SparkSession = getSparkSession(sparkConf)

    def catalog =
        s"""{
           |"table":{"namespace":"default", "name":"spark"},
           |"rowkey":"key",
           |"columns":{
           |"rowkey":{"cf":"rowkey", "col":"key", "type":"string"},
           |"name":{"cf":"info", "col":"name", "type":"string"}
           |}
           |}""".stripMargin

    def withCatalog(cat: String): DataFrame = {
        spark
            .read
            .option("hbase.config.resources", "hbase-site.xml")
            .options(Map(HBaseTableCatalog.tableCatalog -> cat))
            .format("org.apache.spark.sql.execution.datasources.hbase")
            .load()
    }

    val df = withCatalog(catalog)
    df.show()
}
