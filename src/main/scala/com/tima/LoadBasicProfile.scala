package com.tima

import com.tima
import com.tima.spark.Spark
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.execution.datasources.hbase.HBaseTableCatalog

import scala.collection.mutable

object LoadBasicProfile extends App with Spark {
    //    val conf = new SparkConf()
    //    conf.setMaster("local[2]")
    val spark: SparkSession = getSparkSession()
    spark.sparkContext.hadoopConfiguration.set("dfs.nameservices", "tima-cluster")
    spark.sparkContext.hadoopConfiguration.set("dfs.client.failover.proxy.provider.tima-cluster", "org.apache.hadoop.hdfs.server.namenode.ha.ConfiguredFailoverProxyProvider")
    spark.sparkContext.hadoopConfiguration.set("dfs.ha.namenodes.tima-cluster", "nn1,nn2")
    spark.sparkContext.hadoopConfiguration.set("dfs.namenode.rpc-address.tima-cluster.nn1", "hadoop-01:9000")
    spark.sparkContext.hadoopConfiguration.set("dfs.namenode.rpc-address.tima-cluster.nn2", "hadoop-02:9000")

    val dfBasicProfile = spark.read.parquet("hdfs://tima-cluster/data/datafb/output_parquet/profile/20190703_000000/basic_profile")
    val columns = mutable.Map[String, String]()
    val rowKey = "uid"
    val table = "profiles"
    val columnFamily = "info"

    dfBasicProfile.schema.fields.foreach(field => {
        columns(field.name) = field.dataType.typeName
    })

    val schema = Schema(table, columnFamily, key = rowKey, columns = columns)

    def catalog = schema.getHBaseCatalog

    println(catalog)
    val opts = Map(HBaseTableCatalog.tableCatalog -> catalog, HBaseTableCatalog.newTable -> tima.hBaseRegionNumber)

    dfBasicProfile
        .write
        .options(opts)
        .format(tima.hBaseSource).save()

    print("DONE")

}
