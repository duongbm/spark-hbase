package com.tima.spark

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

trait Spark {
    def getSparkSession(conf: SparkConf = null): SparkSession = {
        if (conf == null) {
            SparkSession.builder().getOrCreate()
        } else {
            SparkSession.builder().config(conf).getOrCreate()
        }

    }
}
