name := "SparkLoader"

version := "0.1"

scalaVersion := "2.11.12"
resolvers += "Hortonworks Repository" at "http://repo.hortonworks.com/content/repositories/releases/"
dependencyOverrides += "org.json4s" %% "json4s-jackson" % "3.2.10"
dependencyOverrides += "org.json4s" %% "json4s-core" % "3.2.10"

libraryDependencies ++= Seq(
    "org.apache.spark" %% "spark-sql" % "2.4.0",
    "org.apache.spark" %% "spark-core" % "2.4.0",
    "com.hortonworks" % "shc-core" % "1.1.1-2.1-s_2.11"
)


